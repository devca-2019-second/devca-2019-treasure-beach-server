const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/DevCaDB", {
  useNewUrlParser: true
});

const Events = require("../Models/Events/Events");

const events = [
  {
    name: "Treasure Party",
    date: "1558897200000",
    endTime: "1558918800000",
    TicketPrice: "$ 4000",
    location: {
      long: "787889434",
      lat: "23778238723"
    },
    description: "Enjoy the vibrant lifestyle of treasure beach"
  },

  {
    name: "Callbash",
    date: "1558404000000",
    endTime: "1558411200000",
    TicketPrice: "$ 8000",
    location: {
      long: "787889434",
      lat: "23778238723"
    },
    description:
      "It's here again!! 2019 edition of callabash. Bring your family and enjoy your time in treasure beach"
  },

  {
    name: "Reggae Links",
    date: "1559052000000",
    endTime: "1558411200000",
    TicketPrice: "$ 10000",
    location: {
      long: "787889434",
      lat: "23778238723"
    },
    description: "Enjoy sweet reggae music."
  }
];

const seeder = () => {
  for (i = 0; i < events.length; i++) {
    const event = new Events({
      _id: new mongoose.Types.ObjectId(),
      date: events[i].date,
      endTime: events[i].endTime,
      TicketPrice: events[i].TicketPrice,
      location: {
        long: events[i].location.long,
        lat: events[i].location.lat
      },
      description: events[i].description
    });

    event
      .save()
      .then(() => {
        console.log("stored events successfully");
      })
      .catch(err => {
        console.log(err);
      });
  }
};
module.exports.seeder = seeder;
