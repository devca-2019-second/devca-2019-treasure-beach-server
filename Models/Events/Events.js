const mongoose = require("mongoose");

const EventSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: String,
  date: String,
  endTime: String,
  TicketPrice: String,
  location: {
    long: String,
    lat: String
  },
  description: String
});

module.exports = mongoose.model("Event", EventSchema);
