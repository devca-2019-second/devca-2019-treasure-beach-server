const http = require("http");
const app = require("./app");
const mongoose = require("mongoose");

const port = process.env.PORT || 5000;

const server = http.createServer(app);
server.listen(port, function() {
  console.log("listening port " + port);
});
