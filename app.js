const express = require("express");
const app = express();
const bodyParser = require("body-parser");

const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/DevCaDB", {
  useNewUrlParser: true
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const eventsSeeder = require("./Seeds/EventsSeeder");
const Events = require("./Models/Events/Events");

// eventsSeeder.seeder();

const eventsRoutes = require("./Api/Routes/EventsRoutes");
app.use("/events", eventsRoutes);

app.get("/", (req, res, next) => {
  console.log("Home");
  next();
});

module.exports = app;
