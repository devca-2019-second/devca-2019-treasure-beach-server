const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/DevCaDB", {
  useNewUrlParser: true
});

const Events = require("../../Models/Events/Events");

router.get("/", (req, res, next) => {
  Events.find()
    .exec()
    .then(res => {
      console.log(res);
      res.send({ events: res });
    })
    .catch(err => {
      console.log(err);
    });
});

module.exports = router;
